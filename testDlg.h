// testDlg.h : header file
//

#if !defined(AFX_TESTDLG_H__54331D81_B323_4D24_8132_1A15786BB09B__INCLUDED_)
#define AFX_TESTDLG_H__54331D81_B323_4D24_8132_1A15786BB09B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CTestDlg dialog

class CTestDlg : public CDialog
{
// Construction
public:
	CTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CTestDlg)
	enum { IDD = IDD_TEST_DIALOG };
	CListBox	m_ctlList;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnButton8();
	afx_msg void OnButton9();
	afx_msg void OnButton7();
	afx_msg void OnButton10();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonGetFamer();
	afx_msg void OnEnChangeEditUserNo();
	CString m_userNo;
	CString m_famerId;
	CString m_famerName;
	CString m_phoneNo;
	CString m_waterNo;
	CString m_platformAddr;
	CString m_key;
	CString m_farmerIdCreate;
	CString m_accountMoney;
	CString m_cardNo;
	CString m_phoneNoCreate;
	afx_msg void OnBnClickedButtonCreateLocalCard();
	CString m_newCardNo;
	afx_msg void OnBnClickedButtonModifyCardNo();
	afx_msg void OnBnClickedButtonStartCard();
	afx_msg void OnBnClickedButtonStopCard();
	CString m_money;
	CString m_remark;
	afx_msg void OnBnClickedButtonRecharge();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTDLG_H__54331D81_B323_4D24_8132_1A15786BB09B__INCLUDED_)
