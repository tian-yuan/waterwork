// testDlg.cpp : implementation file
//

#include "stdafx.h"
#include "test.h"
#include "testDlg.h"
#include "MUR500USB.h"
#include "DLL.h"
#include <afxsock.h>
#include "Http\EvppHttpClientManager.h"
#include "rapidjson/document.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
_ConnectionPtr  m_pConnection;
CString str23;
CString str24;
int BUKCISHU;
int startDIS = 0;
CString strTTMP1;
int tempmy = 0;
_RecordsetPtr   m_pRecordset;
_RecordsetPtr   m_pRecordset1;
_RecordsetPtr   m_pRecordset2;
_RecordsetPtr   m_pRecordset3;
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

void int2str(int n, char *str)
{
	char buf[10] = "";
	int i = 0;
	int len = 0;
	int temp = n < 0 ? -n : n;  // temp为n的绝对值

	if (str == NULL)
	{
		return;
	}
	while (temp)
	{
		buf[i++] = (temp % 10) + '0';  //把temp的每一位上的数存入buf
		temp = temp / 10;
	}

	len = n < 0 ? ++i : i;  //如果n是负数，则多需要一位来存储负号
	str[i] = 0;            //末尾是结束符0
	while (1)
	{
		i--;
		if (buf[len - i - 1] == 0)
		{
			break;
		}
		str[i] = buf[len - i - 1];  //把buf数组里的字符拷到字符串
	}
	if (i == 0)
	{
		str[i] = '-';          //如果是负数，添加一个负号
	}
}








class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// Dialog Data
		//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestDlg dialog

CTestDlg::CTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestDlg::IDD, pParent)
	, m_userNo(_T(""))
	, m_famerId(_T(""))
	, m_famerName(_T(""))
	, m_phoneNo(_T(""))
	, m_waterNo(_T(""))
	, m_platformAddr(_T(""))
	, m_key(_T(""))
	, m_farmerIdCreate(_T(""))
	, m_accountMoney(_T(""))
	, m_cardNo(_T(""))
	, m_phoneNoCreate(_T(""))
	, m_newCardNo(_T(""))
	, m_money(_T(""))
	, m_remark(_T(""))
{
	//{{AFX_DATA_INIT(CTestDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestDlg)
	DDX_Control(pDX, IDC_LIST1, m_ctlList);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT_USER_NO, m_userNo);
	DDX_Text(pDX, IDC_EDIT_FAMER_ID, m_famerId);
	DDX_Text(pDX, IDC_EDIT_FAMER_NAME, m_famerName);
	DDX_Text(pDX, IDC_EDIT_PHONE_NO, m_phoneNo);
	DDX_Text(pDX, IDC_EDIT_WATER_NO, m_waterNo);
	DDX_Text(pDX, IDC_EDIT_ADDR, m_platformAddr);
	DDX_Text(pDX, IDC_EDIT_KEY, m_key);
	DDX_Text(pDX, IDC_EDIT_FARMER_ID_CREATE, m_farmerIdCreate);
	DDX_Text(pDX, IDC_EDIT_ACCOUNT_MONEY, m_accountMoney);
	DDX_Text(pDX, IDC_EDIT_CARD_NO, m_cardNo);
	DDX_Text(pDX, IDC_EDIT_PHONE, m_phoneNoCreate);
	DDX_Text(pDX, IDC_EDIT_NEW_CARD_NO, m_newCardNo);
	DDX_Text(pDX, IDC_EDIT_MONEY, m_money);
	DDX_Text(pDX, IDC_EDIT_REMARD, m_remark);
}

BEGIN_MESSAGE_MAP(CTestDlg, CDialog)
	//{{AFX_MSG_MAP(CTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_BUTTON8, OnButton8)
	ON_BN_CLICKED(IDC_BUTTON9, OnButton9)
	ON_BN_CLICKED(IDC_BUTTON7, OnButton7)
	ON_BN_CLICKED(IDC_BUTTON10, OnButton10)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_GET_FAMER, &CTestDlg::OnBnClickedButtonGetFamer)
	ON_EN_CHANGE(IDC_EDIT_USER_NO, &CTestDlg::OnEnChangeEditUserNo)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_LOCAL_CARD, &CTestDlg::OnBnClickedButtonCreateLocalCard)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY_CARD_NO, &CTestDlg::OnBnClickedButtonModifyCardNo)
	ON_BN_CLICKED(IDC_BUTTON_START_CARD, &CTestDlg::OnBnClickedButtonStartCard)
	ON_BN_CLICKED(IDC_BUTTON_STOP_CARD, &CTestDlg::OnBnClickedButtonStopCard)
	ON_BN_CLICKED(IDC_BUTTON_RECHARGE, &CTestDlg::OnBnClickedButtonRecharge)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestDlg message handlers

BOOL CTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	WSADATA wsaData;
	// Initialize Winsock 2.2
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	google::InitGoogleLogging("NetComm");
	google::SetLogDestination(google::GLOG_INFO, "log");

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here


	unsigned long ttemp, gg, ttemp1, ttemp2;
	CString  mm;double dtemp;
	gg = 99;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT1, mm);
	gg = 2;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT23, mm);
	dtemp = 10.89;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT2, mm);
	dtemp = 0.54;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT3, mm);

	gg = 1234;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT18, mm);
	gg = 50002;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT19, mm);

	//mm="183.196.7.131";

	CString strIP = "183.196.7.131";
	DWORD dwIP = ntohl(inet_addr(strIP));
	((CIPAddressCtrl*)GetDlgItem(IDC_IPADDRESS1))->SetAddress(dwIP);

	((CIPAddressCtrl*)GetDlgItem(IDC_IPADDRESS1))->GetAddress(dwIP); //读取
//	unsigned long ttmm=htonl(dwIP); //高低字节反转
  //  DWORD dwIP = ntohl(ttmm);

	mm = "130527";
	SetDlgItemText(IDC_EDIT22, mm);


	mm = "202214";
	SetDlgItemText(IDC_EDIT21, mm);

	gg = 400;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT8, mm);


	mm = "张三";
	SetDlgItemText(IDC_EDIT6, mm);

	gg = 99;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT4, mm);
	gg = 39;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT5, mm);
	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT9, mm);
	dtemp = 0;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT7, mm);
	dtemp = 230.54;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT11, mm);
	gg = 1;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT16, mm);
	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT17, mm);

	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT24, mm);
	gg = 10;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT25, mm);
	gg = 1;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT26, mm);

	CEvppHttpClientManager::getInstance();
	//CEvppHttpClientManager::getInstance()->getFarmer("ip:port", "zzxt6565911", "2023");

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM)dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}

void CTestDlg::OnOK()
{
	// TODO: Add extra validation here

//	CDialog::OnOK();
}

void CTestDlg::OnButton1()
{
	int i = 0; unsigned char mybuf[32];
	char * Ip = "122.114.55.116";
	int Comm = 2000;

	//CALL_BACK_GET_DATA


	HINSTANCE hdll;


	MYregMYdevice exregMYdevice;


	hdll = LoadLibrary("NetComm.dll ");
	if (hdll != NULL)
	{
		exregMYdevice = (MYregMYdevice)GetProcAddress(hdll, "regMYdevice");
	}
	else
	{
		AfxMessageBox("无法加载DLL---NetComm.dll");
		return;


	}
	//UpdateData(true);
	//start run your function
	 //int mm=5;
	i = exregMYdevice();


	// TODO: Add your control notification handler code here
	if (TX2_Init() != 0)
	{
		MessageBox("连接失败", "错误", MB_OK);

	}
	else
	{
		MessageBox("连接成功", "错误", MB_OK);
		startDIS = 1;
	}



	// 初始化COM,创建ADO连接等操作 
	AfxOleInit();
	m_pConnection.CreateInstance(__uuidof(Connection));

	// 在ADO操作中建议语句中要常用try...catch()来捕获错误信息， 
	// 因为它有时会经常出现一些意想不到的错误。jingzhou xu 
	try
	{

		// 打开本地Access库Demo.mdb    
		m_pConnection->Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=JianChe.mdb", "", "", adModeUnknown);
	}
	catch (_com_error e)
	{
		AfxMessageBox("数据库连接失败，确认数据库JianChe.mdb是否在当前路径下!");
	}

	m_pRecordset.CreateInstance(__uuidof(Recordset));
	m_pRecordset1.CreateInstance(__uuidof(Recordset));

	// 在ADO操作中建议语句中要常用try...catch()来捕获错误信息， 
	// 因为它有时会经常出现一些意想不到的错误。jingzhou xu 

	try
	{
		m_pRecordset->Open("SELECT * FROM guan",                // 查询DemoTable表中所有字段                         
			m_pConnection.GetInterfacePtr(),  // 获取库接库的IDispatch指针                        
			adOpenDynamic,
			adLockOptimistic,
			adCmdText);
	}
	catch (_com_error *e)
	{
		AfxMessageBox(e->ErrorMessage());
	}


	///////////////、、、、、、、、、、、、、、、、、、、、、、、、、
	try
	{
		m_pRecordset1->Open("SELECT * FROM accountRecord",                // 查询DemoTable表中所有字段                         
			m_pConnection.GetInterfacePtr(),  // 获取库接库的IDispatch指针                        
			adOpenDynamic,
			adLockOptimistic,
			adCmdText);
	}
	catch (_com_error *e)
	{
		AfxMessageBox(e->ErrorMessage());
	}

	///////////////////////////////////////////////
	/*
	_CommandPtr m_pCommand;
	m_pCommand.CreateInstance("ADODB.Command");
	m_pCommand->ActiveConnection=m_pConnection;
	m_pCommand->CommandText="select * from AllData";
	_RecordsetPtr   m_pRecord;
	m_pRecordset=m_pCommand->Execute(NULL,NULL,adCmdText);

	try
	{
	CString  mm="899";
	m_pRecordset->AddNew();
	//m_pRecordset->put_Collect("村号",999);
	//m_pRecordset->PutCollect("村号",(_bstr_t)mm);
	m_pRecordset->PutCollect("村号", _bstr_t(mm));
	m_pRecordset->PutCollect("用户号", _variant_t(mm));
	//PutCollect("姓名",(_bstr_t)m_UserName);//_variant_t
	m_pRecordset->Update();

	}
	catch(_com_error *e)
	{
		AfxMessageBox(e->ErrorMessage());
	}
	//m_pRecordset->Close();

	*/

}

void CTestDlg::OnButton2()
{
	// TODO: Add your control notification handler code here
	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		return;
	}
	MessageBox("请直接退出即可！", "错误", MB_OK);
}

void CTestDlg::OnButton3()
{
	// TODO: Add your control notification handler code here
	unsigned long snr;
	unsigned long ttemp, ttemp1, ttemp2;
	CString  tt1;
	UINT type;
	UINT size;
	char data[200]; unsigned char data1[200];unsigned char data2[200];
	char szpwd[20] = "ffffffffffff";	//{0xff,0xff,0xff,0xff,0xff,0xff};

	unsigned char * pnow;
	int lennoww; int i, ii;
	unsigned char err;
	float  ttemp88;

	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		//return;
	}

	//	GetDlgItem(IDC_EDIT1)->GetWindowText(data,99);
	GetDlgItemText(IDC_EDIT1, tt1);
	ttemp = atol(tt1);

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT2, tt1);
	ttemp88 = atof(tt1);ttemp = ttemp88 * 100;
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT3, tt1);
	ttemp88 = atof(tt1);ttemp = ttemp88 * 100;

	data1[8] = (ttemp >> 24) & 0xff;
	data1[9] = (ttemp >> 16) & 0xff;
	data1[10] = (ttemp >> 8) & 0xff;
	data1[11] = ttemp & 0xff;

	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;


	Hex2Char(data1, data, 16);

	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
		MessageBox("防碰撞失败", "错误", MB_OK);
	if (TX2_Casc_Select(ANTICOLLISION1, snr, &size) != 0)			//选择卡
		MessageBox("选择卡失败", "错误", MB_OK);
	if (TX2_Auth_Key(0, szpwd, 1) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(4, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(8, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);

	/////////////////写入卡类型、//////////////////
	memset(data1, 0, 16);
	data1[0] = 0x11;
	data1[1] = 0x11;
	data1[2] = 0x11;
	data1[3] = 0x11;


	GetDlgItemText(IDC_EDIT8, tt1);
	ttemp = atof(tt1);

	data1[8] = (ttemp >> 24) & 0xff;
	data1[9] = (ttemp >> 16) & 0xff;
	data1[10] = (ttemp >> 8) & 0xff;
	data1[11] = ttemp & 0xff;


	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(14, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Auth_Key(0, szpwd, 5) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(20, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	//////////////////////////////////////
	/////////////////写入GPRS设置参数、//////////////////
	GetDlgItemText(IDC_EDIT18, tt1);
	ttemp = atol(tt1);

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT19, tt1);
	ttemp = atol(tt1);
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;

	DWORD dwIP;
	((CIPAddressCtrl*)GetDlgItem(IDC_IPADDRESS1))->GetAddress(dwIP); //读取
//	unsigned long ttmm=htonl(dwIP); //高低字节反转
  //  DWORD dwIP = ntohl(ttmm);

	ttemp = dwIP;

	data1[8] = (ttemp >> 24) & 0xff;
	data1[9] = (ttemp >> 16) & 0xff;
	data1[10] = (ttemp >> 8) & 0xff;
	data1[11] = ttemp & 0xff;


	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 4) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(16, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	/////////////////写入GPRS设置参数2、//////////////////
	GetDlgItemText(IDC_EDIT22, tt1);
	ttemp = atol(tt1);

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT21, tt1);
	ttemp = atol(tt1);
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT23, tt1);
	ttemp = atol(tt1);
	data1[8] = (ttemp >> 24) & 0xff;
	data1[9] = (ttemp >> 16) & 0xff;
	data1[10] = (ttemp >> 8) & 0xff;
	data1[11] = ttemp & 0xff;

	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 4) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(17, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);

	/////////////////写入流量底数和常数、//////////////////
	GetDlgItemText(IDC_EDIT24, tt1);
	ttemp = atol(tt1);

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT25, tt1);
	ttemp = atol(tt1);
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT26, tt1);
	ttemp = atol(tt1);
	data1[8] = 0;
	data1[9] = 0;
	data1[10] = 0;
	data1[11] = ttemp & 0xff;

	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 5) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(21, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);




	MessageBox("设置成功！");
}

void CTestDlg::OnButton4()
{
	// TODO: Add your control notification handler code here
	unsigned long snr;
	unsigned long ttemp, ttemp1, ttemp2;

	CString  tt1;
	UINT type;
	UINT size;
	char data[200]; unsigned char data1[200];unsigned char data2[200];
	char szpwd[20] = "ffffffffffff";	//{0xff,0xff,0xff,0xff,0xff,0xff};
	char szpwd2[20] = "334455667788";
	char     DefaultKey[20];
	unsigned char * pnow;
	int lennoww; int i, ii;
	unsigned char err;

	char buf[200];char buf2[200];





	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		return;
	}





	//////////////////////////////////////
	   ///////////--先验证一下卡是否放好，否则直接退出//////START/////////////
	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
			return;
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}

	///////////--先验证一下卡是否放好，否则直接退出//////END/////////////












	m_pRecordset2.CreateInstance(__uuidof(Recordset));

	// 在ADO操作中建议语句中要常用try...catch()来捕获错误信息， 
	// 因为它有时会经常出现一些意想不到的错误。jingzhou xu 



	try
	{
		CString strSql;
		strSql = "nihh";
		_variant_t strSql1;
		GetDlgItemText(IDC_EDIT6, tt1);
		strSql.Format("select * from accountRecord where 用户名称= '%s'", tt1);
		//ttemp=atol(tt1);
		m_pRecordset2->Open(_variant_t(strSql), m_pConnection.GetInterfacePtr(), adOpenDynamic, adLockOptimistic, adCmdText);                 // 查询DemoTable表中所有字段                         
		//!!!!强烈注意上面OPEN()里面的_variant_t(strSql),这个_variant_t(是必须的，否则出错很难处理
		 // 获取库接库的IDispatch指针                        

	}
	catch (_com_error *e)
	{
		AfxMessageBox(e->ErrorMessage());
	}

	_variant_t var;
	CString strName, strAge;
	try
	{
		if (!m_pRecordset2->BOF)
			m_pRecordset2->MoveFirst();
		else
		{
			// AfxMessageBox("表内数据为空"); 
 //------------查询一下有没有村号和用户号都一样的用户名---START------

			m_pRecordset3.CreateInstance(__uuidof(Recordset));
			try
			{
				CString strSql;
				GetDlgItemText(IDC_EDIT4, tt1);
				strSql.Format("select * from accountRecord where 村号= '%s'", tt1);
				//ttemp=atol(tt1);
				m_pRecordset3->Open(_variant_t(strSql), m_pConnection.GetInterfacePtr(), adOpenDynamic, adLockOptimistic, adCmdText);                 // 查询DemoTable表中所有字段                         
				//!!!!强烈注意上面OPEN()里面的_variant_t(strSql),这个_variant_t(是必须的，否则出错很难处理
				 // 获取库接库的IDispatch指针                        

			}
			catch (_com_error *e)
			{
				AfxMessageBox(e->ErrorMessage());
			}

			_variant_t var;
			CString strName, strAge;
			try
			{
				if (!m_pRecordset3->BOF)
					m_pRecordset3->MoveFirst();
				else
				{
					// AfxMessageBox("表内数据为空"); 

					goto sttt2;
				}

				// 读入库中各字段并加入列表框中 
				while (!m_pRecordset3->adoEOF)
				{
					var = m_pRecordset3->GetCollect("用户号");
					if (var.vt != VT_NULL)

					{
						CString strTTMP;
						int bb1, bb2, bb3;
						strTTMP = (LPCSTR)_bstr_t(var);

						GetDlgItemText(IDC_EDIT5, tt1);


						if (strTTMP == tt1)
						{
							AfxMessageBox("已有用户名称开过相同的村号和用户号，请选择其他的村号或者用户号");
							return;
						}

					}


					// m_AccessList.AddString( strName + " --> "+strAge ); 

					m_pRecordset3->MoveNext();
				}
				// m_AccessList.SetCurSel(0); 
			}
			catch (_com_error *e)
			{
				AfxMessageBox(e->ErrorMessage());
			}



			//------------查询一下有没有村号和户号都一样的用户名---END------

		sttt2:

			///////////////////新加入数据、START、、、、、、、
			CString  mm = "899";
			m_pRecordset2->AddNew();
			//m_pRecordset->put_Collect("村号",999);
			//m_pRecordset->PutCollect("村号",(_bstr_t)mm);
			//m_pRecordset1->PutCollect("村号", _bstr_t(str23));
			//m_pRecordset1->PutCollect("用户号", _variant_t(str24));
			GetDlgItemText(IDC_EDIT6, tt1);
			m_pRecordset2->PutCollect("用户名称", _variant_t(tt1));
			GetDlgItemText(IDC_EDIT9, tt1);
			int iE;
			sscanf(tt1, "%d", &iE);

			if (iE != 0)
			{
				AfxMessageBox("新开户补卡次数必须为0，否则不能开户");
				return;
			}
			m_pRecordset2->PutCollect("补卡次数", _variant_t(tt1));
			GetDlgItemText(IDC_EDIT5, tt1);
			m_pRecordset2->PutCollect("用户号", _variant_t(tt1));
			GetDlgItemText(IDC_EDIT4, tt1);
			m_pRecordset2->PutCollect("村号", _variant_t(tt1));



			COleVariant vx(COleDateTime::GetCurrentTime());

			m_pRecordset2->PutCollect("日期", vx);
			//m_pRecordset->PutCollect("time", _variant_t(m_time));
			//PutCollect("姓名",(_bstr_t)m_UserName);//_variant_t
			m_pRecordset2->Update();

			///////////////////新加入数据、END、、、、、、、
			 //AfxMessageBox("新加入数据成功"); 

			goto sttt3;
		}

		// 读入库中各字段并加入列表框中 
		while (!m_pRecordset2->adoEOF)
		{
			var = m_pRecordset2->GetCollect("补卡次数");
			if (var.vt != VT_NULL)

			{
				CString strTTMP, strTTMP1, strTTMP2;
				int bb1, bb2, bb3;
				strTTMP = (LPCSTR)_bstr_t(var);

				GetDlgItemText(IDC_EDIT9, tt1);
				int iA;
				sscanf(tt1, "%d", &iA);

				int iC = iA - 1;
				//   strTTMP1=iC.ToString(); 
				int iD;
				sscanf(strTTMP, "%d", &iD);
				if (iD != iC)
				{
					AfxMessageBox("补卡次数不对,当前补卡次数是" + strTTMP + "必须比当前次数大于1才能开户");
					return;
				}

				var = m_pRecordset2->GetCollect("村号");
				if (var.vt != VT_NULL)
				{
					strTTMP1 = (LPCSTR)_bstr_t(var);

					SetDlgItemText(IDC_EDIT4, strTTMP1);
					AfxMessageBox("本用户已经开过户!村号:" + strTTMP1);

				}

				var = m_pRecordset2->GetCollect("用户号");
				if (var.vt != VT_NULL)
				{
					strTTMP1 = (LPCSTR)_bstr_t(var);

					SetDlgItemText(IDC_EDIT5, strTTMP1);

					AfxMessageBox("本用户已经开过户!用户号:" + strTTMP1);

				}


				//	tt1=tt1+1;
				m_pRecordset2->PutCollect("补卡次数", _variant_t(tt1));
				m_pRecordset2->Update();
			}
			// m_AccessList.AddString( strName + " --> "+strAge ); 

			m_pRecordset2->MoveNext();
		}
		// m_AccessList.SetCurSel(0); 
	}
	catch (_com_error *e)
	{
		AfxMessageBox(e->ErrorMessage());
	}











sttt3:

	//加密时改正过来即可	   


	//	GetDlgItem(IDC_EDIT1)->GetWindowText(data,99);

	ttemp = 0;

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;


	ttemp = 0;  //默认0
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT7, tt1);

	float  ttemp88;
	ttemp88 = atof(tt1);ttemp = ttemp88 * 100;

	data1[8] = (ttemp >> 24) & 0xff;
	data1[9] = (ttemp >> 16) & 0xff;
	data1[10] = (ttemp >> 8) & 0xff;
	data1[11] = ttemp & 0xff;

	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;


	Hex2Char(data1, data, 16);

	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
			return;
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Casc_Select(ANTICOLLISION1, snr, &size) != 0)			//选择卡
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}

#define CARD_SECR	   
#ifdef CARD_SECR 
	//--------------START-------------- //加密时改正过来即可	
	DefaultKey[0] = 0x66; DefaultKey[1] = 0x55;DefaultKey[2] = 0x44;DefaultKey[3] = 0x33;DefaultKey[4] = 0x22;DefaultKey[5] = 0x11;
	//szpwd[0]=23;

	try
	{

		FILE *fp = fopen("mak.SYSTM", "r/r+/a+");
		// mm= fputs("haod",fp);
		fgets(buf, 100, fp);

		fclose(fp);
	}
	catch (_com_error *e)
	{
		AfxMessageBox(e->ErrorMessage());
	}

	for (i = 0;i < 20;i++) szpwd2[i] = 0;

	for (i = 0;i < 12;i++) szpwd2[i] = buf[i];

	if (TX2_Auth_Key(0, szpwd2, 15) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("卡已经加密，你无法使用，请换厂家专用卡片", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(60, data) != 0)			//往第4块写入数据
	{
		MessageBox("卡已经加密，你无法使用，请换厂家专用卡片", "错误", MB_OK);
		return;
	}

#endif            //--------------END-------------- 


	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(9, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(10, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	/////////////////写入卡类型、//////////////////
	memset(data1, 0, 16);
	data1[0] = 0x22;
	data1[1] = 0x22;
	data1[2] = 0x22;
	data1[3] = 0x22;

	GetDlgItemText(IDC_EDIT9, tt1);
	ttemp = atol(tt1);
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;




	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(14, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Auth_Key(0, szpwd, 5) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(20, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	/////////////写入村号和刷卡状态/////////////////////////
	memset(data1, 0, 16);
	GetDlgItemText(IDC_EDIT4, tt1);
	ttemp = atol(tt1);

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;


	GetDlgItemText(IDC_EDIT5, tt1);
	ttemp = atol(tt1);
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;


	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(12, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(13, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	//////////////////////////////////////
	///////////写入总充值金额和已用总金额/////////////////////////
	memset(data1, 0, 16);


	ttemp = 0;

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT7, tt1);


	ttemp88 = atof(tt1);ttemp = ttemp88 * 100;
	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;


	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 1) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(4, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(8, data) != 0)			//往第4块写入数据
	{
		MessageBox("请求失败", "错误", MB_OK);
		return;
	}














	MessageBox("设置成功！");














}

void CTestDlg::OnButton5()
{

	unsigned long temp1;CString str1;CString str2;
	str2 = "";

	//temp1=card_data.testALLN.temp1;

	unsigned long ttemp, ttemp1, ttemp2;int i, ii;
	// TODO: Add your control notification handler code here
	unsigned long snr;double tempgg;
	UINT type;
	UINT size;
	char data[100];
	char data1[100];
	unsigned char data2[100];
	char szpwd[20] = "ffffffffffff";	//{0xff,0xff,0xff,0xff,0xff,0xff};
	char szpwd1[20] = "113355779966";

	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		return;
	}

	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
		MessageBox("防碰撞失败", "错误", MB_OK);
	if (TX2_Casc_Select(ANTICOLLISION1, snr, &size) != 0)			//选择卡
		MessageBox("选择卡失败", "错误", MB_OK);
	///////////////读卡类型/////////////////////
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Read_Enc(14, data) != 0)			//从第4块读取数据
		MessageBox("读取数据失败", "错误", MB_OK);
	Str2Hex(data, data2, 32);
	ttemp = 0;
	ttemp |= data2[0];ttemp = ttemp << 8;
	ttemp |= data2[1];ttemp = ttemp << 8;
	ttemp |= data2[2];ttemp = ttemp << 8;
	ttemp |= data2[3];




	if (ttemp == 0x11111111)  //设置卡
	{

		str1 = "类型: 设置卡";
		str1 += "\n";
		str1 += "\n";
		str1 += "*******常规相关参数：*******";
		str1 += "\n";
		str1 += "\n";
		ttemp = 0;
		ttemp |= data2[8];ttemp = ttemp << 8;
		ttemp |= data2[9];ttemp = ttemp << 8;
		ttemp |= data2[10];ttemp = ttemp << 8;
		ttemp |= data2[11];
		str2.Format("%d", ttemp);str1 += "常数:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		//、、、、、、、、、、、、、、、、、、、、

		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(8, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);

		// 	if(TX2_Read_Enc(13,data1)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);
		// 	if(TX2_Read_Enc(14,data2)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];
		str2.Format("%d", ttemp);str1 += "刷卡的区号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];

		tempgg = ttemp1;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "报警下限:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		ttemp2 = 0;
		ttemp2 |= data2[8];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[9];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[10];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[11];

		tempgg = ttemp2;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "电价:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";
		str1 += "*******GPRS DTU 设置相关参数：*******";
		str1 += "\n";
		str1 += "\n";
		//----读出GPRS1参数并显示出来------
		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 4) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(16, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);

		// 	if(TX2_Read_Enc(13,data1)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);
		// 	if(TX2_Read_Enc(14,data2)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];
		str2.Format("%d", ttemp);str1 += "DTU ID号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];


		str2.Format("%d", ttemp1);str1 += "端口号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		ttemp2 = 0;
		ttemp2 |= data2[8];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[9];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[10];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[11];

		str1 += "IP地址:";

		ttemp2 = htonl(ttemp2); //高低字节反转
		tempgg = ttemp2;
		unsigned char nood = ttemp2 & 0xff;
		str2.Format("%d", nood);
		//	str2.Format("%f",tempgg);
		str1 += str2;
		str1 += ".";
		ttemp2 >>= 8;
		nood = ttemp2 & 0xff;
		str2.Format("%d", nood);
		str1 += str2;
		str1 += ".";
		ttemp2 >>= 8;
		nood = ttemp2 & 0xff;
		str2.Format("%d", nood);
		str1 += str2;
		str1 += ".";
		ttemp2 >>= 8;
		nood = ttemp2 & 0xff;
		str2.Format("%d", nood);
		str1 += str2;

		str1 += "\n";
		str1 += "\n";
		//----读出GPRS2参数并显示出来------
		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 4) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(17, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);

		// 	if(TX2_Read_Enc(13,data1)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);
		// 	if(TX2_Read_Enc(14,data2)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];


		str1 += "河北定义区号:";
		str2.Format("%d", ttemp);
		str1 += str2;
		str2.Format("%d", ttemp1);
		str1 += str2;
		str1 += "\n";
		str1 += "\n";
		//////////////////
		str1 += "\n";
		str1 += "\n";
		str1 += "*******控制器内部通讯ID号，内部流量底数和流量常数：*******";
		str1 += "\n";
		str1 += "\n";
		ttemp1 = 0;
		ttemp1 |= data2[8];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[9];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[10];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[11];
		str1 += "控制器ID号:";

		str2.Format("%d", ttemp1);
		str1 += str2;
		str1 += "\n";
		str1 += "\n";
		//----控制器内部流量底数和流量常数------
		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 5) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(21, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);

		// 	if(TX2_Read_Enc(13,data1)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);
		// 	if(TX2_Read_Enc(14,data2)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];

		ttemp2 = 0;
		ttemp2 |= data2[8];
		ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[9];
		ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[10];
		ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[11];

		str1 += "控制器内部流量底数:";
		str2.Format("%d", ttemp);
		str1 += str2;
		str1 += "\n";
		str1 += "\n";
		str1 += "控制器内部流量常数:";
		str2.Format("%d", ttemp1);
		str1 += str2;
		str1 += "\n";
		str1 += "\n";
		str1 += "流量计选型:";
		str1 += "\n";
		//ttemp2=ttemp2&0xfffffffe;
		if (ttemp2 == 1)  str1 += "1代表超声波流量计";
		if (ttemp2 == 0)  str1 += "0代表电磁流量计";
		str1 += "\n";
		str1 += "\n";
		//////////////////

		str1 += "\n";
		str1 += "\n";
		/////////////////////////////////////////////////////////////
		str1 += "\n";
		str1 += "\n";
		str1 += "*******回读区的数据：*******";
		str1 += "\n";
		str1 += "\n";
		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(9, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);

		// 	if(TX2_Read_Enc(13,data1)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);
		// 	if(TX2_Read_Enc(14,data2)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];
		str2.Format("%d", ttemp);str1 += "区号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];

		tempgg = ttemp1;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "报警下限:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		ttemp2 = 0;
		ttemp2 |= data2[8];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[9];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[10];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[11];

		tempgg = ttemp2;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "电价:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";



		MessageBox(str1);
	}

	else if (ttemp == 0x55555555)  //清零卡
	{

		str1 = "类型: 清零卡";
		str1 += "\n";
		str1 += "\n";

		//、、、、、、、、、、、、、、、、、、、、

		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 1) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(4, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);

		// 	if(TX2_Read_Enc(13,data1)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);
		// 	if(TX2_Read_Enc(14,data2)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];

		tempgg = ttemp;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "已用总金额:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];

		tempgg = ttemp1;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "总充值金额:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		ttemp2 = 0;
		ttemp2 |= data2[8];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[9];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[10];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[11];
		str2.Format("%d", ttemp2);str1 += "区号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		str1 += "\n";
		str1 += "\n";
		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(9, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);

		// 	if(TX2_Read_Enc(13,data1)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);
		// 	if(TX2_Read_Enc(14,data2)!=0)			//从第20块读取数据
		// 		MessageBox("读取数据失败","错误",MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];
		str2.Format("%d", ttemp);str1 += "用户号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];
		str2.Format("%d", ttemp1);str1 += "充值次数:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		ttemp2 = 0;
		ttemp2 |= data2[8];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[9];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[10];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[11];

		tempgg = ttemp2;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "本次充值金额:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		MessageBox(str1);
	}
	else if (ttemp == 0x22222222)  //用户卡
	{

		str1 = "类型: 用户卡";
		str1 += "\n";
		str1 += "\n";

		//、、、、、、、、、、、、、、、、、、、、

		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 1) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(4, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);


		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];

		tempgg = ttemp;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "已用总金额:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];

		tempgg = ttemp1;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "总充值金额:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(9, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);



		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];
		str2.Format("%d", ttemp);str1 += "刷卡状态标记:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];
		str2.Format("%d", ttemp1);str1 += "充值次数:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		ttemp2 = 0;
		ttemp2 |= data2[8];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[9];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[10];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[11];

		tempgg = ttemp2;tempgg = tempgg / 100;
		str2.Format("%f", tempgg);str1 += "本次充值金额:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";
		/////////////////////////////////////////////
		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(12, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);



		ttemp = 0;
		ttemp |= data2[0];ttemp = ttemp << 8;
		ttemp |= data2[1];ttemp = ttemp << 8;
		ttemp |= data2[2];ttemp = ttemp << 8;
		ttemp |= data2[3];
		str2.Format("%d", ttemp);str1 += "区号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";

		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];
		str2.Format("%d", ttemp1);str1 += "用户号:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";
		/////////////////////////////////////
		if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
			MessageBox("验证失败", "错误", MB_OK);
		if (TX2_Read_Enc(14, data) != 0)			//从第4块读取数据
			MessageBox("读取数据失败", "错误", MB_OK);
		//SetDlgItemText(IDC_LIST1,data);
		Str2Hex(data, data2, 32);


		ttemp1 = 0;
		for (i = 0;i < 12;i++)    ttemp1 += data2[i];
		ttemp1 += 0x33;
		ttemp2 = 0;
		ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
		ttemp2 |= data2[15];
		if (ttemp2 != ttemp1)
			MessageBox("数据效验错误", "错误", MB_OK);





		ttemp1 = 0;
		ttemp1 |= data2[4];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[5];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[6];
		ttemp1 = ttemp1 << 8;
		ttemp1 |= data2[7];
		str2.Format("%d", ttemp1);str1 += "补卡次数:";
		str1 += str2;
		str1 += "\n";
		str1 += "\n";


		MessageBox(str1);
	}














}



void CTestDlg::OnButton6()
{

	unsigned long snr;
	unsigned long ttemp, ttemp1, ttemp2;
	CString  tt1;
	UINT type;
	UINT size;
	char data[200]; unsigned char data1[200];unsigned char data2[200];
	char szpwd[20] = "ffffffffffff";	//{0xff,0xff,0xff,0xff,0xff,0xff};

	unsigned char * pnow;
	int lennoww; int i, ii;
	unsigned char err;

	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		return;
	}


	MessageBox("一般不需要清空，直接重复使用即可！", "错误", MB_OK);
	return;


	memset(data1, 0, 16);

	Hex2Char(data1, data, 16);

	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
		MessageBox("防碰撞失败", "错误", MB_OK);
	if (TX2_Casc_Select(ANTICOLLISION1, snr, &size) != 0)			//选择卡
		MessageBox("选择卡失败", "错误", MB_OK);

	if (TX2_Auth_Key(0, szpwd, 1) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(4, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(5, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(6, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);

	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(8, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(9, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(10, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);

	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(12, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(13, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	// if(TX2_Write_Enc(14,data)!=0)			//往第4块写入数据
	 //		MessageBox("写入数据失败","错误",MB_OK);

	if (TX2_Auth_Key(0, szpwd, 4) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(16, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(17, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(18, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);

	if (TX2_Auth_Key(0, szpwd, 5) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	//if(TX2_Write_Enc(20,data)!=0)			//往第4块写入数据
	//		MessageBox("写入数据失败","错误",MB_OK);
	if (TX2_Write_Enc(21, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(22, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);

	if (TX2_Auth_Key(0, szpwd, 6) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(24, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(25, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(26, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);

	if (TX2_Auth_Key(0, szpwd, 7) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(28, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(29, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Write_Enc(30, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);


	MessageBox("清空成功！");

}

void CTestDlg::OnButton8()
{
	// TODO: Add your control notification handler code here
	unsigned long snr;
	unsigned long ttemp, ttemp1, ttemp2;
	CString  tt1;
	UINT type;
	UINT size;
	char data[200]; unsigned char data1[200];unsigned char data2[200];
	char szpwd[20] = "ffffffffffff";	//{0xff,0xff,0xff,0xff,0xff,0xff};

	unsigned char * pnow;
	int lennoww; int i, ii;
	unsigned char err;

	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		return;
	}

	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
		MessageBox("防碰撞失败", "错误", MB_OK);
	if (TX2_Casc_Select(ANTICOLLISION1, snr, &size) != 0)			//选择卡
		MessageBox("选择卡失败", "错误", MB_OK);
	/////////////////写入卡类型、//////////////////
	memset(data1, 0, 16);
	data1[0] = 0x55;
	data1[1] = 0x55;
	data1[2] = 0x55;
	data1[3] = 0x55;

	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(14, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	if (TX2_Auth_Key(0, szpwd, 5) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Write_Enc(20, data) != 0)			//往第4块写入数据
		MessageBox("写入数据失败", "错误", MB_OK);
	//////////////////////////////////////

	MessageBox("设置成功！");
}

void CTestDlg::OnButton9()
{
	unsigned long temp1;CString str1;CString str2;
	str2 = "";
	tempmy = 1;
	//temp1=card_data.testALLN.temp1;

	unsigned long ttemp, ttemp1, ttemp2;int i, ii;
	// TODO: Add your control notification handler code here
	unsigned long snr;
	UINT type;
	UINT size;
	char data[100];
	char data1[100];
	unsigned char data2[100];
	char szpwd[20] = "ffffffffffff";	//{0xff,0xff,0xff,0xff,0xff,0xff};
	char szpwd1[20] = "113355779966";
	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		return;
	}
	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
		MessageBox("防碰撞失败", "错误", MB_OK);
	if (TX2_Casc_Select(ANTICOLLISION1, snr, &size) != 0)			//选择卡
		MessageBox("选择卡失败", "错误", MB_OK);
	///////////////////////////////////
	if (TX2_Auth_Key(0, szpwd, 1) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Read_Enc(4, data) != 0)			//从第4块读取数据
		MessageBox("读取数据失败", "错误", MB_OK);
	Str2Hex(data, data2, 32);
	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data2[i];
	ttemp1 += 0x33;
	ttemp2 = 0;
	ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[15];
	if (ttemp2 != ttemp1)
		MessageBox("数据效验错误", "错误", MB_OK);




	ttemp = 0;
	ttemp |= data2[0];ttemp = ttemp << 8;
	ttemp |= data2[1];ttemp = ttemp << 8;
	ttemp |= data2[2];ttemp = ttemp << 8;
	ttemp |= data2[3];


	double tempgg;
	tempgg = ttemp;tempgg = tempgg / 100;
	str2.Format("%f", tempgg);
	SetDlgItemText(IDC_EDIT14, str2);
	////////////////、、、、、、、、、、//////////////////////
	ttemp = 0;
	ttemp |= data2[4];ttemp = ttemp << 8;
	ttemp |= data2[5];ttemp = ttemp << 8;
	ttemp |= data2[6];ttemp = ttemp << 8;
	ttemp |= data2[7];

	tempgg = ttemp;tempgg = tempgg / 100;
	str2.Format("%f", tempgg);
	SetDlgItemText(IDC_EDIT15, str2);

	////////////////下面是读充电次数//////////////////////	
	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Read_Enc(9, data) != 0)			//从第4块读取数据
		MessageBox("读取数据失败", "错误", MB_OK);
	Str2Hex(data, data2, 32);
	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data2[i];
	ttemp1 += 0x33;
	ttemp2 = 0;
	ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[15];
	if (ttemp2 != ttemp1)
		MessageBox("数据效验错误", "错误", MB_OK);



	ttemp = 0;
	ttemp |= data2[0];ttemp = ttemp << 8;
	ttemp |= data2[1];ttemp = ttemp << 8;
	ttemp |= data2[2];ttemp = ttemp << 8;
	ttemp |= data2[3];
	str2.Format("%d", ttemp);
	SetDlgItemText(IDC_EDIT13, str2);

	ttemp = 0;
	ttemp |= data2[4];ttemp = ttemp << 8;
	ttemp |= data2[5];ttemp = ttemp << 8;
	ttemp |= data2[6];ttemp = ttemp << 8;
	ttemp |= data2[7];
	str2.Format("%d", ttemp);
	SetDlgItemText(IDC_EDIT10, str2);
	ttemp++;
	str2.Format("%d", ttemp);
	SetDlgItemText(IDC_EDIT16, str2);;
	////////////////下面是用户名和村号//////////////////////	
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Read_Enc(13, data) != 0)			//从第4块读取数据
		MessageBox("读取数据失败", "错误", MB_OK);
	Str2Hex(data, data2, 32);
	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data2[i];
	ttemp1 += 0x33;
	ttemp2 = 0;
	ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[15];
	if (ttemp2 != ttemp1)
		MessageBox("数据效验错误", "错误", MB_OK);



	ttemp = 0;
	ttemp |= data2[0];ttemp = ttemp << 8;
	ttemp |= data2[1];ttemp = ttemp << 8;
	ttemp |= data2[2];ttemp = ttemp << 8;
	ttemp |= data2[3];
	str23.Format("%d", ttemp);//村号
	ttemp = 0;
	ttemp |= data2[4];ttemp = ttemp << 8;
	ttemp |= data2[5];ttemp = ttemp << 8;
	ttemp |= data2[6];ttemp = ttemp << 8;
	ttemp |= data2[7];
	str24.Format("%d", ttemp);//户号

	////////////////下面是补卡次数//////////////////////	
	if (TX2_Auth_Key(0, szpwd, 3) != 0)		//验证第1扇区的密钥A
		MessageBox("验证失败", "错误", MB_OK);
	if (TX2_Read_Enc(14, data) != 0)			//从第4块读取数据
		MessageBox("读取数据失败", "错误", MB_OK);
	Str2Hex(data, data2, 32);
	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data2[i];
	ttemp1 += 0x33;
	ttemp2 = 0;
	ttemp2 |= data2[12];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[13];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[14];ttemp2 = ttemp2 << 8;
	ttemp2 |= data2[15];
	if (ttemp2 != ttemp1)
		MessageBox("数据效验错误", "错误", MB_OK);



	ttemp = 0;
	ttemp |= data2[4];ttemp = ttemp << 8;
	ttemp |= data2[5];ttemp = ttemp << 8;
	ttemp |= data2[6];ttemp = ttemp << 8;
	ttemp |= data2[7];
	//补卡次数

	BUKCISHU = ttemp;

	//	((CButton*)GetDlgItem(IDC_CHECK1))->SetCheck(TRUE);
	((CButton*)GetDlgItem(IDC_CHECK1))->SetCheck(FALSE);
	//	IDC_CHECK1.SetCheck(BST_CHECKED);

}

void CTestDlg::OnButton7()
{
	// TODO: Add your control notification handler code here
	unsigned long snr;
	unsigned long ttemp, ttemp1, ttemp2;
	CString  tt1;
	UINT type;
	UINT size;
	char data[200]; unsigned char data1[200];unsigned char data2[200];
	char szpwd[20] = "ffffffffffff";	//{0xff,0xff,0xff,0xff,0xff,0xff};



	char szpwd2[20] = "334455667788";
	char     DefaultKey[20];

	char buf[200];char buf2[200];

	unsigned char * pnow;
	int lennoww; int i, ii;
	unsigned char err;
	if (startDIS != 1)
	{
		MessageBox("请先点连接按钮，连接成功后再试！", "错误", MB_OK);
		return;
	}

	if (tempmy == 0)
	{
		MessageBox("请注意，充值前先点上面的读回按键", "错误", MB_OK);
		return;

	}




	tempmy = 0;

	//	GetDlgItem(IDC_EDIT1)->GetWindowText(data,99);
	GetDlgItemText(IDC_EDIT13, tt1);
	ttemp = atol(tt1);
	if (ttemp != 0)
	{
		MessageBox("请注意，你的刷卡状态标记不正确，不能正常充值", "错误", MB_OK);
		return;
	}
	GetDlgItemText(IDC_EDIT17, tt1);
	ttemp = atol(tt1);
	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT16, tt1);
	ttemp = atol(tt1);
	GetDlgItemText(IDC_EDIT10, tt1);
	ttemp2 = atol(tt1);
	ttemp2++;  //每次充值，次数加1
	if (ttemp2 != ttemp)
	{
		MessageBox("请注意，你的充值次数不对，不能正常充值", "错误", MB_OK);
		return;
	}

	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;

	GetDlgItemText(IDC_EDIT11, tt1);
	float  ttemp88;
	ttemp88 = atof(tt1);ttemp = ttemp88 * 100;
	data1[8] = (ttemp >> 24) & 0xff;
	data1[9] = (ttemp >> 16) & 0xff;
	data1[10] = (ttemp >> 8) & 0xff;
	data1[11] = ttemp & 0xff;

	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;


	Hex2Char(data1, data, 16);

	if (TX2_Request(0, &type) != 0) 		//进行请求操作
	{
		if (TX2_Request(0, &type) != 0) 		//如果第一次请求失败，再重复请求一次
		{
			MessageBox("请求失败", "错误", MB_OK);
			return;
		}

	}
	if (TX2_Casc_Anticoll(0, ANTICOLLISION1, &snr) != 0)			//进行防碰撞选择，成功则返回卡号
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}
	if (TX2_Casc_Select(ANTICOLLISION1, snr, &size) != 0)			//选择卡
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}



#define CARD_SECR	   
#ifdef CARD_SECR 
	//--------------START-------------- //加密时改正过来即可	
	DefaultKey[0] = 0x88; DefaultKey[1] = 0x77;DefaultKey[2] = 0x66;DefaultKey[3] = 0x55;DefaultKey[4] = 0x44;DefaultKey[5] = 0x33;
	//szpwd[0]=23;
	try
	{

		FILE *fp = fopen("mak.SYSTM", "r/r+/a+");
		// mm= fputs("haod",fp);
		fgets(buf, 100, fp);

		fclose(fp);
	}
	catch (_com_error *e)
	{
		AfxMessageBox(e->ErrorMessage());
	}

	for (i = 0;i < 20;i++) szpwd2[i] = 0;

	for (i = 0;i < 12;i++) szpwd2[i] = buf[i];


	if (TX2_Auth_Key(0, szpwd2, 15) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("卡已经加密，你无法使用，请换厂家专用卡片", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(60, data) != 0)			//往第4块写入数据
	{
		MessageBox("卡已经加密，你无法使用，请换厂家专用卡片", "错误", MB_OK);
		return;
	}




#endif            //--------------END-------------- 








	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(9, data) != 0)			//往第4块写入数据
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}
	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}
	if (TX2_Write_Enc(10, data) != 0)			//往第4块写入数据
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}







	CString  mm = "899";
	m_pRecordset->AddNew();
	//m_pRecordset->put_Collect("村号",999);
	//m_pRecordset->PutCollect("村号",(_bstr_t)mm);
	m_pRecordset->PutCollect("村号", _bstr_t(str23));
	m_pRecordset->PutCollect("用户号", _variant_t(str24));
	GetDlgItemText(IDC_EDIT11, tt1);
	m_pRecordset->PutCollect("本次充值金额", _variant_t(tt1));
	GetDlgItemText(IDC_EDIT16, tt1);
	m_pRecordset->PutCollect("充值次数", _variant_t(tt1));
	GetDlgItemText(IDC_EDIT17, tt1);
	m_pRecordset->PutCollect("充值状态", _variant_t(tt1));

	if (((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck())
	{
		mm = "1";
		m_pRecordset->PutCollect("补卡标志", _variant_t(mm));
		//AfxMessageBox("选定啦");
	}
	else
	{
		mm = "0";
		m_pRecordset->PutCollect("补卡标志", _variant_t(mm));
		//AfxMessageBox("没有选定啦");
	}

	//tt1.Format("%s",BUKCISHU);
	//CString bb;
	//int2str(BUKCISHU, bb);
	tt1.Format("%d", BUKCISHU);//
	m_pRecordset->PutCollect("补卡次数", _variant_t(tt1));

	COleVariant vx(COleDateTime::GetCurrentTime());

	m_pRecordset->PutCollect("data", vx);
	//m_pRecordset->PutCollect("time", _variant_t(m_time));
	//PutCollect("姓名",(_bstr_t)m_UserName);//_variant_t
	m_pRecordset->Update();



	//为防治写数据库错误，导致数据错误，把写入冲值额放在数据库操作下面完成
	memset(data1, 0, 16);
	GetDlgItemText(IDC_EDIT14, tt1);

	ttemp88 = atof(tt1);ttemp = ttemp88 * 100;

	data1[0] = (ttemp >> 24) & 0xff;
	data1[1] = (ttemp >> 16) & 0xff;
	data1[2] = (ttemp >> 8) & 0xff;
	data1[3] = ttemp & 0xff;


	GetDlgItemText(IDC_EDIT15, tt1);

	ttemp88 = atof(tt1);ttemp = ttemp88 * 100;


	GetDlgItemText(IDC_EDIT11, tt1);

	ttemp88 = atof(tt1);ttemp2 = ttemp88 * 100;

	ttemp = ttemp + ttemp2;

	data1[4] = (ttemp >> 24) & 0xff;
	data1[5] = (ttemp >> 16) & 0xff;
	data1[6] = (ttemp >> 8) & 0xff;
	data1[7] = ttemp & 0xff;



	ttemp1 = 0;
	for (i = 0;i < 12;i++)    ttemp1 += data1[i];
	ttemp1 += 0x33;
	data1[12] = (ttemp1 >> 24) & 0xff;
	data1[13] = (ttemp1 >> 16) & 0xff;
	data1[14] = (ttemp1 >> 8) & 0xff;
	data1[15] = ttemp1 & 0xff;

	Hex2Char(data1, data, 16);
	if (TX2_Auth_Key(0, szpwd, 1) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}


	if (TX2_Write_Enc(4, data) != 0)			//往第4块写入数据
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}


	if (TX2_Auth_Key(0, szpwd, 2) != 0)		//验证第1扇区的密钥A
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}


	if (TX2_Write_Enc(8, data) != 0)			//往第4块写入数据
	{
		MessageBox("写入数据失败", "错误", MB_OK);
		return;
	}







	MessageBox("设置成功！");
}

void CTestDlg::OnButton10()
{

	unsigned long ttemp, gg, ttemp1, ttemp2;
	CString  mm;double dtemp;
	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT1, mm);
	dtemp = 0;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT2, mm);
	dtemp = 0;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT3, mm);


	gg = 400;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT8, mm);


	mm = "";
	SetDlgItemText(IDC_EDIT6, mm);

	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT4, mm);
	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT5, mm);
	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT9, mm);
	dtemp = 0;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT7, mm);
	dtemp = 0;
	mm.Format("%f", dtemp);
	SetDlgItemText(IDC_EDIT11, mm);
	gg = 1;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT16, mm);
	gg = 0;
	mm.Format("%d", gg);
	SetDlgItemText(IDC_EDIT17, mm);







	// TODO: Add your control notification handler code here

}


void CTestDlg::OnBnClickedButtonGetFamer()
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	CString platformAddr = m_platformAddr;
	CString key = m_key;
	CString userNo = m_userNo;
	std::string result = CEvppHttpClientManager::getInstance()->getFarmer(platformAddr.GetBuffer(), key.GetBuffer(), userNo.GetBuffer());
	std::string response = UTF8ToAnsi(result);
	if (response.empty()) {
		AfxMessageBox("获取农户信息失败！");
	} else {
		rapidjson::Document document;
		if (document.ParseInsitu((char*)response.c_str()).HasParseError()) {
			AfxMessageBox("解析农户信息失败！");
			return;
		}
		if (!document.HasMember("code")) {
			AfxMessageBox("获取农户信息失败！");
			return;
		}
		int code = 0;
		if (document["code"].IsInt()) {
			code = document["code"].GetInt();
		}
		else {
			std::string code = document["code"].GetString();
			code = atoi(code.c_str());
		}
		
		std::string msg = document["msg"].GetString();
		if (code != 0) {
			AfxMessageBox(msg.c_str());
		} else {
			const auto& data = document["data"].GetObject();
			m_famerId = data["farmId"].GetString();
			m_famerName = data["name"].GetString();
			m_phoneNo = data["phone"].GetString();
			m_waterNo = data["waterNo"].GetString();
			UpdateData(false);
		}
	}
}


void CTestDlg::OnEnChangeEditUserNo()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}


void CTestDlg::OnBnClickedButtonCreateLocalCard()
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	CString platformAddr = m_platformAddr;
	CString key = m_key;
	CString farmerIdCreate = m_farmerIdCreate;
	CString accountMoney = m_accountMoney;
	CString cardNo = m_cardNo;
	CString phoneCreate = m_phoneNoCreate;
	std::string result = CEvppHttpClientManager::getInstance()->createLocalCard(platformAddr.GetBuffer(), key.GetBuffer(),
		m_farmerIdCreate.GetBuffer(), accountMoney.GetBuffer(), cardNo.GetBuffer(), phoneCreate.GetBuffer());

	if (result.empty()) {
		AfxMessageBox("发放水权卡失败！");
	}
	else {
		std::string response = UTF8ToAnsi(result);
		rapidjson::Document document;
		if (document.ParseInsitu((char*)response.c_str()).HasParseError()) {
			AfxMessageBox("解析发放水权卡返回消息失败！");
			return;
		}
		if (!document.HasMember("code")) {
			AfxMessageBox("发放水权卡失败！");
			return;
		}
		int code = 0;
		if (document["code"].IsInt()) {
			code = document["code"].GetInt();
		}
		else {
			std::string code = document["code"].GetString();
			code = atoi(code.c_str());
		}
		std::string msg = document["msg"].GetString();
		if (code != 0) {
			AfxMessageBox(msg.c_str());
		} else {
			AfxMessageBox("发放水权卡成功！");
		}
	}
}


void CTestDlg::OnBnClickedButtonModifyCardNo()
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	CString platformAddr = m_platformAddr;
	CString key = m_key;
	CString cardNo = m_cardNo;
	CString newCardNo = m_newCardNo;
	std::string result = CEvppHttpClientManager::getInstance()->modifyCardNo(platformAddr.GetBuffer(), key.GetBuffer(),
		cardNo.GetBuffer(), newCardNo.GetBuffer());
	if (result.empty()) {
		AfxMessageBox("修改水权卡失败！");
	}
	else {
		std::string response = UTF8ToAnsi(result);
		rapidjson::Document document;
		if (document.ParseInsitu((char*)response.c_str()).HasParseError()) {
			AfxMessageBox("解析修改水权卡返回消息失败！");
			return;
		}
		if (!document.HasMember("code")) {
			AfxMessageBox("修改水权卡失败！");
			return;
		}
		int code = 0;
		if (document["code"].IsInt()) {
			code = document["code"].GetInt();
		}
		else {
			std::string code = document["code"].GetString();
			code = atoi(code.c_str());
		}
		std::string msg = document["msg"].GetString();
		if (code != 0) {
			AfxMessageBox(msg.c_str());
		}
		else {
			AfxMessageBox("修改水权卡成功！");
		}
	}
}


void CTestDlg::OnBnClickedButtonStartCard()
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	CString platformAddr = m_platformAddr;
	CString key = m_key;
	CString cardNo = m_cardNo;
	CString newCardNo = m_newCardNo;;
	std::string result = CEvppHttpClientManager::getInstance()->status(platformAddr.GetBuffer(), key.GetBuffer(),
		cardNo.GetBuffer(), "true", "");
	if (result.empty()) {
		AfxMessageBox("启用水权卡失败！");
	}
	else {
		std::string response = UTF8ToAnsi(result);
		rapidjson::Document document;
		if (document.ParseInsitu((char*)response.c_str()).HasParseError()) {
			AfxMessageBox("解析启用水权卡返回消息失败！");
			return;
		}
		if (!document.HasMember("code")) {
			AfxMessageBox("启用水权卡失败！");
			return;
		}
		int code = 0;
		if (document["code"].IsInt()) {
			code = document["code"].GetInt();
		}
		else {
			std::string code = document["code"].GetString();
			code = atoi(code.c_str());
		}
		std::string msg = document["msg"].GetString();
		if (code != 0) {
			AfxMessageBox(msg.c_str());
		}
		else {
			AfxMessageBox("启用水权卡成功！");
		}
	}
}


void CTestDlg::OnBnClickedButtonStopCard()
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	CString platformAddr = m_platformAddr;
	CString key = m_key;
	CString cardNo = m_cardNo;
	CString newCardNo = m_newCardNo;;
	std::string result = CEvppHttpClientManager::getInstance()->status(platformAddr.GetBuffer(), key.GetBuffer(),
		cardNo.GetBuffer(), "false", "");
	if (result.empty()) {
		AfxMessageBox("停用水权卡失败！");
	}
	else {
		std::string response = UTF8ToAnsi(result);
		rapidjson::Document document;
		if (document.ParseInsitu((char*)response.c_str()).HasParseError()) {
			AfxMessageBox("解析停用水权卡返回消息失败！");
			return;
		}
		if (!document.HasMember("code")) {
			AfxMessageBox("停用水权卡失败！");
			return;
		}
		int code = 0;
		if (document["code"].IsInt()) {
			code = document["code"].GetInt();
		}
		else {
			std::string code = document["code"].GetString();
			code = atoi(code.c_str());
		}
		std::string msg = document["msg"].GetString();
		if (code != 0) {
			AfxMessageBox(msg.c_str());
		}
		else {
			AfxMessageBox("停用水权卡成功！");
		}
	}
}


void CTestDlg::OnBnClickedButtonRecharge()
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	CString platformAddr = m_platformAddr;
	CString key = m_key;
	CString cardNo = m_cardNo;
	CString money = m_money;
	CString remark = m_remark;
	std::string result = CEvppHttpClientManager::getInstance()->recharge(platformAddr.GetBuffer(), key.GetBuffer(),
		cardNo.GetBuffer(), money.GetBuffer(), remark.GetBuffer());
	if (result.empty()) {
		AfxMessageBox("水权卡充值失败！");
	}
	else {
		std::string response = UTF8ToAnsi(result);
		rapidjson::Document document;
		if (document.ParseInsitu((char*)response.c_str()).HasParseError()) {
			AfxMessageBox("解析水权卡充值返回消息失败！");
			return;
		}
		if (!document.HasMember("code")) {
			AfxMessageBox("水权卡充值失败！");
			return;
		}
		int code = 0;
		if (document["code"].IsInt()) {
			code = document["code"].GetInt();
		}
		else {
			std::string code = document["code"].GetString();
			code = atoi(code.c_str());
		}
		std::string msg = document["msg"].GetString();
		if (code != 0) {
			AfxMessageBox(msg.c_str());
		}
		else {
			AfxMessageBox("水权卡充值成功！");
		}
	}
}
