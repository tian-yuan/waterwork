//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by test.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TEST_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_BUTTON4                     1003
#define IDC_EDIT1                       1004
#define IDC_LIST1                       1005
#define IDC_EDIT2                       1006
#define IDC_EDIT3                       1007
#define IDC_BUTTON5                     1008
#define IDC_BUTTON6                     1009
#define IDC_EDIT4                       1010
#define IDC_EDIT5                       1011
#define IDC_EDIT6                       1012
#define IDC_EDIT7                       1013
#define IDC_EDIT8                       1014
#define IDC_BUTTON7                     1015
#define IDC_EDIT9                       1016
#define IDC_BUTTON8                     1017
#define IDC_BUTTON9                     1018
#define IDC_SLIDER1                     1019
#define IDC_EDIT12                      1020
#define IDC_CHECK1                      1020
#define IDC_BUTTON10                    1021
#define IDC_IPADDRESS1                  1022
#define IDC_EDIT18                      1023
#define IDC_EDIT19                      1024
#define IDC_EDIT10                      1025
#define IDC_EDIT11                      1026
#define IDC_EDIT13                      1027
#define IDC_EDIT14                      1028
#define IDC_EDIT15                      1029
#define IDC_EDIT20                      1030
#define IDC_EDIT16                      1031
#define IDC_EDIT17                      1032
#define IDC_EDIT21                      1033
#define IDC_EDIT22                      1034
#define IDC_EDIT23                      1035
#define IDC_EDIT24                      1036
#define IDC_EDIT25                      1037
#define IDC_EDIT26                      1038
#define IDC_EDIT27                      1039
#define IDC_EDIT28                      1040
#define IDC_EDIT29                      1041
#define IDC_EDIT30                      1042
#define IDC_BUTTON11                    1043
#define IDC_BUTTON_GET_FAMER            1044
#define IDC_EDIT31                      1045
#define IDC_EDIT_FARMER_ID_CREATE       1045
#define IDC_EDIT32                      1046
#define IDC_EDIT_CARD_NO                1046
#define IDC_EDIT_USER_NO                1047
#define IDC_EDIT_FAMER_ID               1048
#define IDC_EDIT_FAMER_NAME             1049
#define IDC_EDIT_PHONE_NO               1050
#define IDC_EDIT_WATER_NO               1051
#define IDC_EDIT_ADDR                   1052
#define IDC_EDIT39                      1053
#define IDC_EDIT_KEY                    1053
#define IDC_EDIT_ACCOUNT_MONEY          1054
#define IDC_EDIT_PHONE                  1055
#define IDC_BUTTON_CREATE_LOCAL_CARD    1056
#define IDC_EDIT_NEW_CARD_NO            1057
#define IDC_BUTTON_MODIFY_CARD_NO       1058
#define IDC_BUTTON_START_CARD           1059
#define IDC_BUTTON_STOP_CARD            1060
#define IDC_EDIT_MONEY                  1061
#define IDC_EDIT_REMARD                 1062
#define IDC_BUTTON16                    1063
#define IDC_BUTTON_RECHARGE             1063

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1064
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
