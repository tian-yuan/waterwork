
#include "stdafx.h"

#include "Utils.h"
#include <string>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <cstdint>
using namespace std;

time_t getCurrentTime() {
	time_t curr = time(NULL);
	return curr;
}

wstring UTF8ToUnicode(const string & s)
{
	wstring result;

	int n = MultiByteToWideChar(CP_UTF8, 0, s.c_str(), -1, NULL, 0);
	wchar_t * buffer = new wchar_t[n];

	::MultiByteToWideChar(CP_UTF8, 0, s.c_str(), -1, buffer, n);

	result = buffer;
	delete[] buffer;

	return result;
}


std::string WChar2Ansi(LPCWSTR pwszSrc)
{
	int nLen = 0;
	nLen = WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, NULL, 0, NULL, NULL);
	if (nLen <= 0)
		return std::string("");

	char* pszDst = new char[nLen];
	if (NULL == pszDst)
		return std::string("");

	WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, pszDst, nLen, NULL, NULL);
	pszDst[nLen - 1] = 0;
	std::string strTemp(pszDst);
	delete[] pszDst;

	return strTemp;
}

std::string UTF8ToAnsi(const std::string& s) {
	std::wstring str = UTF8ToUnicode(s);
	return WChar2Ansi(str.c_str());
}

const int MAX_STRSIZE = 1024;
std::string ANSItoUTF8(std::string instr)	//ansi-->utf-8
{
	WCHAR wcharstr[MAX_STRSIZE];
	memset(wcharstr, 0, MAX_STRSIZE);
	MultiByteToWideChar(CP_ACP, 0, (LPCSTR)instr.c_str(), -1, wcharstr, MAX_STRSIZE);
	char charstr[MAX_STRSIZE];
	memset(charstr, 0, MAX_STRSIZE);
	WideCharToMultiByte(CP_UTF8, 0, wcharstr, -1, charstr, MAX_STRSIZE, NULL, NULL);
	return charstr;
}

const std::string & HEX_2_NUM_MAP()
{
	static const std::string str("0123456789ABCDEF");
	return str;
}

const std::string & ASCII_EXCEPTION()
{
	static const std::string str(R"("%<>[\]^_`{|})");
	return str;
}

unsigned char NUM_2_HEX(const char h, const char l)
{
	unsigned char hh = std::find(std::begin(HEX_2_NUM_MAP()), std::end(HEX_2_NUM_MAP()), h) - std::begin(HEX_2_NUM_MAP());
	unsigned char ll = std::find(std::begin(HEX_2_NUM_MAP()), std::end(HEX_2_NUM_MAP()), l) - std::begin(HEX_2_NUM_MAP());
	return (hh << 4) + ll;
}

std::string UrlEncode(const std::string & url)
{
	std::string ret;
	for (auto it = url.begin(); it != url.end(); ++it)
	{
		if (((*it >> 7) & 1) || (std::count(std::begin(ASCII_EXCEPTION()), std::end(ASCII_EXCEPTION()), *it)))
		{
			ret.push_back('%');
			ret.push_back(HEX_2_NUM_MAP()[(*it >> 4) & 0x0F]);
			ret.push_back(HEX_2_NUM_MAP()[*it & 0x0F]);
		}
		else
		{
			ret.push_back(*it);
		}
	}
	return ret;
}

std::string UrlDecode(const std::string & url)
{
	std::string ret;
	for (auto it = url.begin(); it != url.end(); ++it)
	{
		if (*it == '%')
		{
			if (std::next(it++) == url.end())
			{
				throw std::invalid_argument("url is invalid");
			}
			ret.push_back(NUM_2_HEX(*it, *std::next(it)));
			if (std::next(it++) == url.end())
			{
				throw std::invalid_argument("url is invalid");
			}
		}
		else
		{
			ret.push_back(*it);
		}
	}
	return ret;
}
