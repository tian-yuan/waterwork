#pragma once

#include <time.h>
#include <string>

time_t getCurrentTime();

std::string UTF8ToAnsi(const std::string& s);

std::string UrlEncode(const std::string & url);
std::string UrlDecode(const std::string & url);
std::string ANSItoUTF8(std::string instr);