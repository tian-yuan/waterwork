#pragma once

#include "EvppHttpClient.h"
#include "Utils/Utils.h"
#include "Utils/MD5.h"

#include <string.h>

class CEvppHttpClientManager {
public:
	static CEvppHttpClientManager* getInstance() {
 	  static CEvppHttpClientManager* gHttpClient = NULL;
	  static std::mutex lock;
	  lock.lock();
	  if (gHttpClient == NULL) {
	    gHttpClient = new CEvppHttpClientManager();
	  }
	  lock.unlock();
	  return gHttpClient;
	}

public:
	std::string request(std::string url) {
		return m_httpClient.request(url);
	}

public:
	std::string getFarmer(std::string url, std::string key, std::string userNo) {
		std::string contentType = "application/x-www-form-urlencoded";
		std::string path = "http://" + url + "/a/record/api/getFarmer?";
		time_t ts = getCurrentTime();
		char tsStr[32];
		snprintf(tsStr, sizeof(tsStr), "%d", ts);
		std::string signedStr = "key=" + key + "&timestamp=" + tsStr + "&userNo=" + userNo;
		std::string sign = md5(signedStr);
		LOG_INFO << "signed string :" << signedStr;
		LOG_INFO << "sign : " << sign;
		std::map<std::string, std::string> header;
		header["Content-type"] = "application/x-www-form-urlencoded";
		path += signedStr + "&sign=" + sign;
		return m_httpClient.post(path, "1", header);
	}

	std::string createLocalCard(std::string url, std::string key, std::string farmId, std::string accountMoney, 
		std::string cardNo, std::string phone) {
		std::string contentType = "application/x-www-form-urlencoded";
		std::string path = "http://" + url + "/a/record/api/createLocalCard?";
		time_t ts = getCurrentTime();
		char tsStr[32];
		snprintf(tsStr, sizeof(tsStr), "%d", ts);
		std::string signedStr = "accountMoney=" + accountMoney + "&cardNo=" + cardNo + 
			"&farmId=" + farmId + "&key=" + key + "&phone=" + phone + "&timestamp=" + tsStr;
		std::string sign = md5(signedStr);
		LOG_INFO << "signed string :" << signedStr;
		LOG_INFO << "sign : " << sign;
		std::map<std::string, std::string> header;
		header["Content-type"] = "application/x-www-form-urlencoded";
		path += signedStr + "&sign=" + sign;
		return m_httpClient.post(path, "", header);
	}

	std::string modifyCardNo(std::string url, std::string key, std::string cardNo, std::string newCardNo) {
		std::string contentType = "application/x-www-form-urlencoded";
		std::string path = "http://" + url + "/a/record/api/modifyCardNo?";
		time_t ts = getCurrentTime();
		char tsStr[32];
		snprintf(tsStr, sizeof(tsStr), "%d", ts);
		std::string signedStr = "cardNo=" + cardNo + "&key=" + key + "&newCardNo=" + newCardNo + "&timestamp=" + tsStr;
		//std::string signedStr = "cardNo=" + cardNo + "&newCardNo=" + newCardNo + "&timestamp=" + tsStr;
		std::string sign = md5(signedStr);
		LOG_INFO << "signed string :" << signedStr;
		LOG_INFO << "sign : " << sign;
		std::map<std::string, std::string> header;
		header["Content-type"] = "application/x-www-form-urlencoded";
		path += signedStr + "&sign=" + sign;
		return m_httpClient.post(path, "", header);
	}

	std::string status(std::string url, std::string key, std::string cardNo, std::string enable, std::string newCardNo) {
		std::string contentType = "application/x-www-form-urlencoded";
		std::string path = "http://" + url + "/a/record/api/status?";
		time_t ts = getCurrentTime();
		char tsStr[32];
		snprintf(tsStr, sizeof(tsStr), "%d", ts);
		std::string signedStr = "cardNo=" + cardNo + "&enable=" + enable + "&key=" + key + "&timestamp=" + tsStr;
		//std::string signedStr = "cardNo=" + cardNo + "&enable=" + enable + "&timestamp=" + tsStr;
		std::string sign = md5(signedStr);
		LOG_INFO << "signed string :" << signedStr;
		LOG_INFO << "sign : " << sign;
		std::map<std::string, std::string> header;
		header["Content-type"] = "application/x-www-form-urlencoded";
		path += signedStr + "&sign=" + sign;
		return m_httpClient.post(path, "", header);
	}

	std::string recharge(std::string url, std::string key, std::string cardNo, std::string money, std::string remark) {
		std::string contentType = "application/x-www-form-urlencoded";
		std::string path = "http://" + url + "/a/record/api/recharge?";
		time_t ts = getCurrentTime();
		char tsStr[32];
		snprintf(tsStr, sizeof(tsStr), "%d", ts);
		std::string signedStr = "cardNo=" + cardNo +"&key=" + key + "&money=" + money +
			"&remark=" + ANSItoUTF8(remark) + "&timestamp=" + tsStr;
		std::string sign = md5(signedStr);
		LOG_INFO << "signed string :" << signedStr;
		LOG_INFO << "sign : " << sign;
		std::map<std::string, std::string> header;
		header["Content-type"] = "application/x-www-form-urlencoded";
		path += signedStr + "&sign=" + sign;
		return m_httpClient.post(UrlEncode(path), "", header);
	}

private:
	CEvppHttpClient m_httpClient;
};

