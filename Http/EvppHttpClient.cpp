
#include "stdafx.h"

#include "EvppHttpClient.h"
#include "../Utils/Utils.h"
#include <evpp/httpc/conn.h>
#include <future>
#include <map>

CEvppHttpClient::CEvppHttpClient() {
	m_loop.Start(true);
}

CEvppHttpClient::~CEvppHttpClient() {
	m_loop.Stop(true);
}


static void HandleHTTPResponse(const std::shared_ptr<evpp::httpc::Response>& response,
	evpp::httpc::Request* request, std::promise<std::string>& resPromise) {
	LOG_INFO << "handler http response;";
	if (request != NULL) {
		delete request;   // The request MUST BE deleted in EventLoop thread.
	}
	if (response.get() == NULL) {
		LOG_ERROR << "handler http response is empty.";
		resPromise.set_value("");
		return;
	}
	LOG_INFO << "handler http response : " << response->body().ToString();
	resPromise.set_value(response->body().ToString());
}

std::string CEvppHttpClient::request(std::string url) {
	LOG_INFO << "http request url : " << url;
	evpp::httpc::GetRequest* r = new evpp::httpc::GetRequest(m_loop.loop(), url, evpp::Duration(5.0));
	
	std::promise<std::string> resPromise;
	std::future<std::string> resFuture = resPromise.get_future();
	r->Execute(std::bind(&HandleHTTPResponse, std::placeholders::_1, r, std::ref(resPromise)));
	std::string response = resFuture.get();
	LOG_INFO << "http request get response : " << response;
	return response;
}

std::string CEvppHttpClient::post(std::string url, std::string body, std::map<std::string, std::string> header) {
	evpp::httpc::PostRequest* r = new evpp::httpc::PostRequest(m_loop.loop(), url, body, evpp::Duration(5.0));
	for (std::map<std::string, std::string>::iterator iter = header.begin(); iter != header.end(); ++iter) {
		r->AddHeader(iter->first, iter->second);
	}
	std::promise<std::string> resPromise;
	std::future<std::string> resFuture = resPromise.get_future();
	r->Execute(std::bind(&HandleHTTPResponse, std::placeholders::_1, r, std::ref(resPromise)));
	std::string response = resFuture.get();
	LOG_INFO << "http request get response : " << response;
	return response;
}
