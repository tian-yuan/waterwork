#pragma once

#include <string>
#include <evpp/event_loop_thread.h>
#include <evpp/httpc/request.h>
#include <evpp/httpc/response.h>
#include <future>
#include <map>

class CEvppHttpClient {
public:
	CEvppHttpClient();
	~CEvppHttpClient();

public:
	std::string request(std::string url);
	std::string post(std::string url, std::string body, std::map<std::string, std::string> header);

private:
  evpp::EventLoopThread m_loop;	
};

